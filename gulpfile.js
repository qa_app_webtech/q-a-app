var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var sass = require('gulp-sass');

// minify css
gulp.task('minify-css', function() {
	console.log("minify-css");
	return gulp.src('public/stylesheets/css/*.css')
	    .pipe(minifyCss({compatibility: 'ie8'}))
	    .pipe(gulp.dest('dist'));
});

// images minify
gulp.task('imagemin', function () {
	console.log("image minify");
    return gulp.src('public/images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('sass', function () {
	console.log("sass");
    gulp.src('./scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./css'));
});

gulp.task('develop', function () {
  nodemon({ script: 'bin/www'
          , ext: 'js'
          })
    .on('restart', function () {
      console.log('restarted!')
    })
})

gulp.task('default', ['minify-css', 'imagemin', 'sass', 'develop']);