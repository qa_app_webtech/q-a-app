var mongoose = require('mongoose');
var userSchema = mongoose.Schema({
	facebook: {
		id: String,
		token: String,
		email: String,
		name: String,
		admin:  {type: Number, default: 0}
	}
});

module.exports = mongoose.model('User', userSchema);