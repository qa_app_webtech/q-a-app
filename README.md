# Q&A - applicatie #

#### Team: ####

Jonas Reymen 
&
Kevin Corne

#### Wat doet onze applicatie? ####

- Vraagt locatie op
- Vragen locken
- Facebook login
- Users kunnen vragen aanmaken en reageren op deze vragen
- URL vraag kopiëren
- Drukt vragen en antwoorden af, reageren op een vraag is live upgedate
- Zoekt vragen in de buurt
- vragen worden voorzien van auteur + moment.js voor het laten zien hoeveel minuten geleden een post gemaakt is
- links naar afbeeldingen worden automatisch getoond
- alles is live
- websockets
- mongodb : 
	- 2 tabellen 
		- facebookuser
		- vragen + antwoorden
- css3 animatie:
	- bij append van de antwoorden
	- logo homepage