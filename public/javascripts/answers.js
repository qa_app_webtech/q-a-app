$(document).ready(function(){
	var socket = io.connect('http://localhost:3000');

	//url
	var url = document.URL;

	// title
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	//deelbare link
	$('#url').attr('value', url);
	var clip = new ZeroClipboard($("#copyurl"), {
    moviePath: "zeroclipboard-2.1.6/dist/ZeroClipboard.swf"});


	//getting the id from the question out of the url
	function getUrlVars() {
	    var vars = {};
	    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	        vars[key] = value;
	    });
	    return vars;
	}
	var questionid = getUrlVars()["id"];
	socket.emit('pushingid', questionid);

	//picture
	$('#picture').attr("id", questionid);
	$('#picturehref').attr("id", "href" + questionid);

	

	//on click making an object to push it to database
	$('#btn').on('click', function(){
		var answer = $('#answer').val();
		var auteur = $('#hidden').val();
		var answers = {'answer': answer, 'auteur': auteur, 'questionid': questionid};
		console.log(answers);
		if(answer == "") {
			$('#errormessage').html('Dit veld moet ingevuld zijn.');
		} else {
			socket.emit('answer', answers);
			$('#errormessage').html("");
		}
		$('#answer').val("");
	});

	socket.on('answers', function(question){
		$('#question').html(question[0].title);
		$('#information').html(question[0].information);
		$('#auteur').html('door ' + question[0].auteur);
		if (question[0].picture == "") {
			$('#'+ questionid).remove();
			$('#href' + questionid).remove();
		} else {
			$('#'+ questionid).attr("src", question[0].picture);
			$('#href' + questionid).attr("href", question[0].picture);
		};	
	});

	socket.on('user', function(user){
		console.log(user);
	});

	//showing answers when loading the page
	socket.on('answers', function(allanswers){
		if(allanswers[0].locked === true){
			$('#verdwijn' + questionid).remove();
		}
		try{

			for (var i = 0; i < allanswers[0].answers.length; i++) {
				$("#showanswers").append('<li class="list-group-item"><p>' + allanswers[0].answers[i].answer + '</p><p class="auteur">Door ' + allanswers[0].answers[i].auteur + '</p></li></br>');
			};

		} catch (err){
			console.log("no answers")
		}

	});

	//live update
	socket.on('updateanswers', function(update){
		$("#showanswers").append('<li id="appendli" class="list-group-item"><p>' + update.answer + '</p><p class="auteur">Door ' + update.auteur + '</p></li></br>');
	});

	$('#verdwijn').attr("id", "verdwijn"+questionid);

	$('#lock').on('click', function(){
		var lock = true;
		var locked = {'lock': lock, 'questionid': questionid};
		socket.emit('lock', locked);
		$('#verdwijn' + questionid).remove();
	});

	var admin = $('#admin').val();
	if(admin == 0){
		$('#lock').remove();
	}

});