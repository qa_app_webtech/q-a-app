$(document).ready(function(){
	var socket = io.connect('http://localhost:3000');

	var message;
	getLocation();

	function getmessagecoords(latitude, longitude){

		var coords = {
			'longitude': longitude,
			'latitude': latitude
		};

		console.log(coords);

		var title = $('#title').val();
		var auteur = $('#hidden').val();
		var information = $('#information').val();
		var picture = $('#picture').val();
		var question = {'title': title, 'information': information, 'auteur': auteur, 'coords': coords, 'picture': picture};

		if(picture.match(/\.(jpeg|jpg|gif|png)$/) !== null || picture == ""){
			console.log(question);
			socket.emit('ask', question);
			$('#post').hide(1000);
			$('#errormessage').html("");
			$('#information').val("");
			$('#title').val("");
			$('#picture').val("");
		} else {
			$('#errormessage').html("De opgegeven url is geen foto.");
		}	
	}

	$('#showquestions').on('click', '.question', function(){
		var id = $(this).attr('id');
		console.log(id);
		socket.emit('id', id);
	});

	socket.on('indebuurt', function(data){
		console.log(data);
		$("#showquestions").empty();
		for (var i = 0; i < data.length; i++) {
			$("#showquestions").prepend('<li class="list-group-item"><a href="/answers?id=' + data[i]._id + '">' + data[i].title + '</a>' + '<div id="crop">' + data[i].information + '</div>' + '<p> Geplaatst door ' + data[i].auteur + ' - ' + moment(data[i].date).fromNow() + '</p></li></br>');
	 	}
	});

	//show questions
	socket.on('questions', function(allquestions){
		console.log(allquestions);
		for (var i = 0; i < allquestions.length; i++) {
			$("#showquestions").prepend('<li class="list-group-item"><a href="/answers?id=' + allquestions[i]._id + '">' + allquestions[i].title + '</a>' + '<div id="crop">' + allquestions[i].information + '</div>' + '<p> Geplaatst door ' + allquestions[i].auteur + ' - ' + moment(allquestions[i].date).fromNow() + '</p></li></br>');
		};
	});

	socket.on('updatequestions', function(data){
		$('#showquestions').prepend('<li class="list-group-item" id="uwvraag"><a href="/answers?id=' + data._id + '" class="question">' + data.title + '</a>' + '<div id="crop">' + data.information + '</div>' + '<p> Geplaatst door ' + data.auteur + ' - ' + moment(data.date).fromNow() + '</p></li></br>');
	});

	//geolocation
	function getLocation(message) {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(showPosition);
	    } else {
	        console.log("Geolocation is not supported by this browser.");
	    }
	}

	function showPosition(position) {
	    var latitude = position.coords.latitude;
	    var longitude = position.coords.longitude;
	    var coordinaten = {"latitude": latitude, "longitude": longitude};

	    $('#ask').on('click', function(){
			getmessagecoords(latitude, longitude);
		});

		$('#buurt').on('click', function(){
			socket.emit("buurt", coordinaten);
		});
	}
		$('#post').hide();
		$('#postquestion').on("click", function(){
		$('#post').toggle(1000);
	});
});