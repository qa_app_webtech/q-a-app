var express = require('express');
var router = express.Router();
var passport = require('passport');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express'});
});

router.get('/about', function(req, res, next){
	res.render('about');
});

router.get('/ask_a_question', function(req, res, next) {
	if (req.user) {
        res.render('questions', {user: req.user});
    } else {
    	res.redirect('/');
    }
});

router.get('/answers', function(req, res, next) {
	if (req.user) {
        res.render('answers', {user: req.user});
    } else {
    	res.redirect('/');
    }
  
});

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

function loggedIn(req, res, next) {
    if (req.user) {
        next();
    } else {
        res.redirect('/login');
    }
}

router.get('/auth/facebook', passport.authenticate('facebook', {scope: ['email']}));

	router.get('/auth/facebook/callback', 
	  passport.authenticate('facebook', { successRedirect: '/ask_a_question',
	                                      failureRedirect: '/' }));

module.exports = router;
