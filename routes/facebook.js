var express = require('express');
var router = express.Router();
var passport = require('passport');


	router.get('/auth/facebook', passport.authenticate('facebook', {scope: ['email']}));

	router.get('facebook/auth/facebook/callback', 
	  passport.authenticate('facebook', { successRedirect: '/ask_a_question',
	                                      failureRedirect: '/' }));
module.exports = router;